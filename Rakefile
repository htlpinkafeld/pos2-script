namespace :book do
  desc 'prepare build'
  task :prebuild do
    Dir.mkdir 'images' unless Dir.exists? 'images'
    Dir.glob("source/*/images/*").each do |image|
      FileUtils.copy(image, "images/" + File.basename(image))
    end
    FileUtils.cp_r('avatars', 'images/')
  end

  desc 'build HTML version'

  file 'theme/html/htlpi.css' =>
    ['asciidoctor-stylesheet-factory/sass/settings/_htlpi.scss',
     'asciidoctor-stylesheet-factory/sass/htlpi.scss'] do
    puts "Compiling htlpi.scss"
    file `cd asciidoctor-stylesheet-factory && ./build-stylesheet.sh htlpi && cp stylesheets/htlpi.css ../theme/html/`
    Dir.mkdir 'fonts/Lato' unless Dir.exists? 'fonts/Lato'
    file `cp -r asciidoctor-stylesheet-factory/sass/fonts/lato-offline/fonts/* fonts/Lato`
    puts " -- stylesheet copied to theme/html/htlpi.css"
  end

  task :stylesheet => ['theme/html/htlpi.css']

  task :html => [:prebuild,:stylesheet] do
    puts "Converting to HTML..."
    `bundle exec asciidoctor -r asciidoctor-diagram -a stylesheet=theme/html/htlpi.css -a html pos2script.adoc`
    puts " -- HTML output at pos2script.html"
  end

  desc 'build epub version'
  task :epub => :prebuild do
    puts "Converting to EPub..."
    `bundle exec asciidoctor-epub3 -r asciidoctor-diagram pos2script.adoc`
    puts " -- Epub output at pos2script.epub"
  end

  desc 'build mobi version'
  task :mobi => :prebuild do
    puts "Converting to Mobi (kf8)..."
    `bundle exec asciidoctor-epub3 -r asciidoctor-diagram -a ebook-format=kf8 pos2script.adoc`
    puts " -- Mobi output at pos2script.mobi"
  end

  desc 'build pdf version'
  task :pdf => :prebuild do
    puts "Converting to PDF... (this one takes a while)"
    `bundle exec asciidoctor-pdf -r asciidoctor-diagram -a pdf-stylesdir=theme/pdf -a pdf-style=htlpi -a pdf-fontsdir=theme/fonts pos2script.adoc`
    puts " -- PDF  output at pos2script.pdf"
  end

  desc 'build zip file of html version'
  task :zip => :html do
    puts "Zipping ... "
    `rm pos2script.html.zip`
    `zip -r pos2script.html.zip pos2script.html css images fonts`
    puts " -- ZIP output at pos2script.html.zip"
  end

  desc 'build all versions'
  task :build => [:html, :epub, :mobi, :pdf, :zip]
end

task :default => "book:build"
task :watch do
    sh 'bundle exec guard -o 10.0.2.2:4000'
end

task :listen do
    sh 'listen -r -f 0.0.0.0:4000 -d source'
end

task :serve do
    sh 'gulp serve'
end

[[subsec:bitshift_operators]]
=== Bit-Shift Operators

With the operators from the previous section, we can modify individual bits **in place**.
With the binary bit-shift operators `<<` and `>>` we can also **shift** the bit pattern of a whole number **to the left or right** by a certain number of positions.

==== Bit-Shift Left

****
WARNING: The bit-shift left operator `x << n` shifts the bit pattern of `x` by `n` positions to the left.
The missing bits on the right-hand side are filled with zeros.
****

[NOTE]
Shifting the bits of a number by one position to the left corresponds to a multiplication by 2.
The reason for that is: the significance of every `1` in the number changes from 2^i^ to 2^i+1^ (= 2 * 2^i^).
Therefore, shifting the bits by `n` positions to the left corresponds to a multiplication by 2^n^!

[example]
====
*Example*
[source,c]
....
char x = 4;         // 0000 0100 <1>
x = x << 1;         // 0000 1000 = 8 <2>
x = 3;              // 0000 0011 <3>
x = x << 2;         // 0000 1100 = 12 = 3 * 2^2 <4>

// attention
x = 127;            // 0111 1111
x = x << 1;         // 1111 1110 <5>
printf("%d", x);    // prints -2 (two's complement!)
....
<1> A variable `x` with data type `char` (remember: 1 byte) is defined and assigned a value of 4, which consists of one `1` in the position 2^2^.
<2> The bit pattern is shifted by one position to the left, the result is 8 (4 * 2).
<3> The value 3 is assigned to x, which consists of two ``1``s.
<4> Shifting `x` by two positions to the left, corresponds to a multiplication by 2^2^ = 4, such that result is 12.
<5> The number 127 is shifted by one to the left. If you print the value of `x` afterwards, it will be -2.
Reason: `x` is interpreted using the two's complement, because it is defined as a `signed` variable, which can also hold negative numbers (vs. `unsigned` variables consisting only of positive numbers).
The first bit of the number being 1 indicates a negative number, and using the two's complement (`0000 0001` -> `0000 0010`) we end up with -2.
====


[WARNING]
Note that the left shift is **NOT circular**, that means if you shift bits off at the left hand side (because the data type is not large enough to hold the bits), they are **lost** and cannot be recovered.
Moreover, if you shift bits off at the left hand side, `x << n` does *not* correspond to a multiplication by 2^n^ any longer.

[example]
====
*Example*
[source,c]
....
char x = 127;       // 0111 1111
x = x << 8;         // 0000 0000
printf("%d", x);    // prints 0, all bits are "lost".
....
====

==== Bit-Shift Right

Bit-shift right is very similar to bit-shift left (of course, in the opposite direction) -- shifting a variable by `n` bits to the right corresponds to a (whole number) division by 2^n^.

[example]
====
*Example*
[source,c]
....
char x = 10;    // 0000 1010
x = x >> 1;     // 0000 0101 = 5 dec <1>
x = x >> 1;     // 0000 0010 = 2 dec <2>
....
<1> The value 10 is shifted one bit to the right and results in 5.
The left-most bit is filled with a `0`.
<2> The value 5 is again shifted one bit to the right.
The right-most `1` is shifted "off" (is lost), which corresponds to losing the remainder of the division by 2.
The result is therefore 2.
====

[TIP]
.What happens with negative numbers?
====
If you shift a negative number to the right, the result varies from compiler to compiler.
Usually, the compiler fills up the left-most bit with a `1` in that case, so that the operation still corresponds to a division by 2^n^.
====

[example]
====
*Example*
[source,c]
....
x = -127;       // 1000 0001 (two's complement!)
x = x >> 1;     // 1100 0000 = -64
....
====

NOTE: The result of a shift operation is undefined if the right operand is negative or larger than the number of bits of the left operand.

[example]
====
*Example*

[source,c]
....
char x = 5;
x = x << -1;        // undefined operation!
printf("%d\n", x);  // here: 0
x = 5;
x = x << 9;         // undefined operation!
printf("%d\n", x);  // here: 0
....
====

NOTE: There are also the shortcut operators `<\<=` and `>>=`.

===== Example

Converting binary input (a character sequence of ``0``s and ``1``s) to a decimal number:

[source,c,numbered]
....
#include<stdio.h>
#include<ctype.h>

int main() {
  char c;
  unsigned int value = 0;
  while((c = getch()) != 13)
  {
    if(c == '0' || c == '1') {
       putchar(c);
       value <<= 1;
       value |= (c - '0');
    }
  }
  printf("\nDecimal value = %d",value);
  return 0;
}
....

The important lines are number 11 and 12: the old `value` is shifted by one bit to the left.
The new input is converted from ASCII code (`'0'` or `'1'`) to a decimal value (0 or 1) by subtracting the ASCII value of `'0'`.
Then the new bit is added on the right hand side using a bitwise OR operation (the right-most bit is replaced by the input).



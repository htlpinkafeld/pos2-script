[[subsec:structs]]
=== Structures

A *structure* (*struct*, sometimes also called *record*) is a data type that combines different (primitive or compound) data types into a new one.
In contrast to arrays, each component (also called _member_) of a struct can have a different type.
A member can itself be a primitive type, an array, a struct, a union or an enumeration (we will talk about the unions and enumerations later).

==== Definition and Usage of Structs

There are three different ways of defining a struct, and we will go through all of them, because you will need to known them at some time.

===== Declaration Variant 1

The first way of defining a struct is using the `struct` keyword, followed by an identifier (the name of the struct), a list of all members in curly braces and a semicolon.

[source,c]
....
struct Date {
    char day;
    char month[4];
    int year;
};
....

By convention, we name structs in upper camel case (e.g., `StudentData`).
The block in curly braces is called the member definition.
Just like variable definitions, each member consists of a data type and a name.
In this case, we defined a structure to hold the information of a *single date*.
It consists of three members:

* `day` is a `char` and can hold a day number like `23`
* `month` is a string and can hold a month abbreviation like `"JAN"`
* and `year` is an int and can hold a year number like `2016`.

To *use* this struct in a program, we need to define a variable like follows:

[source,c]
....
struct Date {
    char day;
    char month[4];
    int year;
};
int main() {
    struct Date d1;
}
....

Again, the keyword `struct` must be used, followed by the name of the structure. `struct Date` is the data type of the variable `d1`.

With this variable declaration, the compiler reserves 9 bytes of memory. Why?

* `char day`: 1 byte
* `char month[4]`: 4 bytes
* `int year`: 4 bytes (assuming an int has 4 bytes)

A total of 9 bytes are needed to store all three members.
In memory, the three members are stored next to each other -- at least you can imagine it that way (sometimes the compiler may decide to leave some space between the members for optimization reasons).

So `d1` is a name for this 9 byte object in memory:

[graphviz,generated_structs_declaration1,svg]
....
digraph struct {
    rankdir=LR;
    node[shape=record,fontname=Helvetica];
    d1;
    str [label="<f0>char day| char month[4]|int year"];
    d1 -> str:f0;
}
....

===== Accessing Struct Members

Because a struct consists of multiple components with different data types, you cannot access (read or modify) a struct as a whole.
Instead, you have to access the members individually using the dot syntax (called _member access operator_):

[source,c]
....
struct Date d1;
d1.day = 23; // <1>
strcpy(d1.month, "JAN"); // <2>
d1.year = 2016; // <3>

printf("%d %s %d", d1.day, d1.month, d1.year); // <4>

// WRONG:
d1 = 23;           // WRONG, which member should be written?
printf("%d", d1);  // WRONG, which member should be used?
....
<1> The first member, `day` is assigned a value of 23. You have to write the name of the variable (`d1`), followed by a dot (`.`) and the name of the member (`day`) to access the right memory.
Only the first byte of `d1` is changed.
<2> Because `month` is an array, we cannot assign a string using the assignment operator (`=`), but we have to use `strcpy`. Byte 2--5 are changed.
<3> Only the last four bytes of `d1` are changed to the integer value 2016.
<4> When reading values from `d1`, we also have to specify which member should be read.
In this case, to print the date, we have to use three format specifies (according to the data types of the members) and access all three members individually!

[WARNING]
====
When accessing struct members, you have to write the name of the variable (`d1`), followed by a dot (`.`) and the name of the member (`day`, `month`, `year`).
====

[[subsubsec:struct_decl_var2]]
===== Declaration Variant 2

The second variant is very similar to the first one.
However, instead of defining the struct globally, we can also define it directly inside a variable declaration like in the following example:

[source,c]
....
int main () {
    struct {
        char day;
        char month[4];
        int year;
    } d2;
    d2.day = 31;
    ....
}
....

In this variant, we do not define a name for the struct, but use the definition syntax to directly define a variable `d2` with the same three members as before.
The members `day`, `month` and `year` can be accessed in the same way as above.

This variant is often used when nesting structs inside structs or unions (see section <<subsubsec:nesting_structures>> below). These structs are sometimes also called anonymous structs as they have no name.

[WARNING]
====
The disadvantage of this variant is that you cannot define a second variable of the same data type as `d2` later in your program without repeating the struct definition (because the struct has no name).
====

===== Declaration Variant 3

Because we are actually defining a new data type (and because typing the `struct` keyword over and over again is cumbersome), we usually use `typedef` to declare a new data type from the struct:

[source,c]
....
typedef struct {
    char day;
    char month[4];
    int year;
} TDate;
....

To understand that definition remember the general `typedef` structure:

`typedef <oldtype> <newtype>;`

* `<oldtype>` is our struct (in this case without a name, because we are defining a new type name anyway):
`struct { char day; char month[4]; int year; }`

* `<newtype>` is our new type name: `TDate`.
By convention, we prefix those type names with `T`.

We can now use the new type without the struct keyword:

[source,c]
....
int main() {
    TDate d3;
    scanf("%d", &d3.day);
    fgets(d3.month, 4, stdin);
    scanf("%d", &d3.year);
}
....


[[subsubsec:nesting_structures]]
==== Nesting Structures

As already mentioned above, a member of a struct can be a struct as well.
This comes in handy if you want to define more complex data types.

For example, we could define a type to store information about a person as follows.

[source,c]
....
typedef struct {
    char day;
    char month[4];
    int year;
} TDate;
typedef struct {
    char firstname[3][21];
    char lastname[41];
} TName;
typedef struct {
    TName name;
    TDate birthdate;
} TPerson;
....

In addition to the `TDate` struct from above, we define a struct for a name.
As a name typically consists of multiple parts, we define those parts as separate members.
In our case, we want to store up to three first names, each of which can be up to 20 characters long, and a last name which can be up to 40 characters long.
You can see that we can also use two-dimensional arrays as members.
Finally, we define a struct `TPerson` with two members `name` and `birthdate`.
For those members we can simply use the new types that we defined immediately above.

If we define a variable `TPerson p1`, we can imagine the following memory structure to be reserved:

[graphviz,generated_structs_nested_person,svg]
....
digraph struct {
    rankdir=LR;
    node[shape=record,fontname=Helvetica];
    date [label="<f0>char day| char month[4]|int year"];
    name [label="<f0>char firstname[3][21]|char lastname[41]"];
    person [label="<f0>TName name|<f1>TDate birthdate"];
    person:f0 -> name:f0;
    person:f1 -> date:f0;
    p1 -> person:f0;
}
....

To access the members of `p1`, we have to use multiple dots now, e.g.:

[source,c]
....
int main() {
    TPerson p1;
    ...
    puts(p1.name.firstname[0]); // prints the first firstname
    ...
    p1.birthdate.day = 13;  // changes the person's birthday
    p1.birthdate.year = 1999;
    ...
}
....

[WARNING]
====
Again, accessing structs as a whole makes no sense. For example `printf("%d", p1.birthdate)` addresses a whole structure, but the compiler does not know how to print the data type `TDate` or how to convert it to an integer (due to the `%d`).
====

While the picture above provides a good explanation on how to access a person's members, the memory layout is a little bit different.
Again, all members are stored next to each other in memory, so the memory layout would look more like that:

[graphviz,generated_structs_nestedperson_memory,svg]
....
digraph struct {
    rankdir=LR;
    node[shape=record,fontname=Helvetica];
    person [label="<f0>char firstname[3][21]|<f1>char lastname[41]|<f2>char day|<f3>char month[4]|<f4>int year"];
    p1 -> person:f0;
}
....

So there is actually no additional memory needed for `TName name` and `TDate birthdate` other than for their members.

Knowing that, we can also calculate the size of a `TPerson` variable:

* `char firstname[3][21]`: 63 bytes
* `char lastname[41]`: 41 bytes
* `char day`: 1 byte
* `char month[4]`: 4 bytes
* `int year`: 4 bytes

Total: 113 bytes. You can also check that using your compiler:

[source,c]
....
... // struct definitions from above
int main() {
    printf("%d", sizeof(TPerson));  // 113
}
....

As mentioned, depending on your compiler, this may print a number larger than 113 if the compiler decides to stuff a few additional bytes between your members to make the start addresses multiples of 8/16/32/64/...

As a shorthand for the two notations from above, from here on we will draw nested structures like that:

[graphviz,generated_structs_nestedperson_combined,svg]
....
digraph struct {
    rankdir=LR;
    node[shape=record,fontname=Helvetica];
    person [label="{<f0>TName name|{<f1>char firstname[3][21]|char lastname[41]}}|{TDate birthdate
        |{<f2>char day|<f3>char month[4]|<f4>int year}}"];
    p1 -> person:f0;
}
....


We can also use the anonymous struct declaration syntax to define the `TPerson` struct in one declaration:

[source,c]
....
typedef struct {
    struct {
        char firstname[3][21];
        char lastname[41];
    } name;
    struct {
        char day;
        char month[4];
        int year;
    } birthdate;
} TPerson;
....

In this case, the structures for the name and birthdate are not defined separately and thus cannot be used in other definitions.

[NOTE]
====
Do not forget that in `struct { ... } x` the name `x` denotes a variable name (see <<subsubsec:struct_decl_var2>> above). Therefore it makes no sense to use a typename like `TName` instead of `name` in the definition above. `name` is a member of the struct `TPerson`!
====

[[subsubsec:struct_assignment]]
==== Struct Assignment

You are allowed to copy struct variables using the assignment operator:

[source,c]
....
int main() {
    TPerson p1, p2;
    p1.birthdate.day = 30;
    ...
    p2 = p1;
    printf("%d", p2.birthdate.day); // will print 30
}
....

For `p2 = p1`, the compiler will basically insert a `memcpy(p2, p1, sizeof(TPerson))`.
That means, `p2` will be a byte-by-byte copy of `p1`.

[WARNING]
====
Comparing struct variables using `==` is not defined! A condition like `if (p1 == p2)` will usually throw a compiler error. You have to check equality by comparing each member individually!
====


==== Passing Structs to Functions

Passing a struct variable to a function works like passing a primitive data type variable.
Since the compiler knows how to copy a struct (see <<subsubsec:struct_assignment>>), the variable is passed **by value**, that means, the function receives a *copy* of it.
Also, for the same reason, a struct can be used as a return value, as seen in the following example:

[source,c]
....
typedef struct {
    char day;
    char month[4];
    int year;
} TDate;

TDate readDate();
void printDate(TDate d);

int main(void){
    TDate d = readDate();
    printDate(d);
    return 0;
}

TDate readDate() {
    TDate d;
    printf("Day: ");
    scanf("%d", &d.day);
    printf("Month: ");
    fflush(stdin);
    fgets(d.month, 4, stdin);
    printf("Year: ");
    scanf("%d", &d.year);
    return d;
}

void printDate(TDate d) {
    printf("%d-%s-%d\n", d.day, d.month, d.year);
}
....

[WARNING]
.Passing large structs to functions involves copying a lot of data!
====
Do not pass large structs using call-by-value, because copying large amounts of data is slow and unnecessary in most cases. Use input/output parameters instead!
====

Example for using structs as input/output parameters:

[source,c]
....
void upCaseMonth(TDate* d); // <1>

int main(void){
    TDate d = readDate();
    upCaseMonth(&d); // <2>
    printDate(d);
    return 0;
}

void upCaseMonth(TDate* d) {
    int i = 0;
    while((*d).month[i]) {  // <3>
        (*d).month[i] = toupper((*d).month[i]);
        i++;
    }
}
....
<1> The parameter `d` of `upCaseMonth` is an input/output parameter.
As usual, we need to write a ``*`` sign in front of the variable name in the parameter list.
This means that the function does **not** receive a copy of the total struct, but just its address.
So `d` is an address in this function.
<2> Consequently, we need to *pass* the *address* of our struct variable when we call `upCaseMonth`.
Therefore, we write the address-of operator (`&`) in front of the variable name.
<3> In the function itself, we need to dereference `d` before accessing it.
Note that the member access operator (`.`) has a higher priority than the dereference operator (`*`) according to the operator precedence table.
Therefore, we need to write `(*d).month` (dereference `d` and then select `month`) instead of `*d.month` (select `month` in an address and then dereference it -- makes no sense).

[WARNING]
====
Always use `(*ioparam).member` when using input/output parameters.
====

[NOTE]
====
When using a member from an input/output parameter for scanf, do not forget the additional `&`:

`scanf("%d", &(*ioparam).member)`
====

===== Shorthand for Member Access on Pointers

Typing `(*x).y` is cumbersome but necessary very often.
Therefore, there is a shorthand "arrow operator" (officially also called _member access operator_):

[TIP]
====
`x\->y` is the same as `(*x).y`
====

The left hand side of the `\->` operator is not interpreted as a structure, but as the address of a structure.

The `upCaseMonth` function from above could be rewritten as follows:

[source,c]
....
void upCaseMonth(TDate* d) {
    int i = 0;
    while(d->month[i]) {
        d->month[i] = toupper(d->month[i]);
        i++;
    }
}
....

Of course, this operator can also be used with scanf:

`scanf("%d", &ioparam\->member)`




==== Initializing Structs

Initializing each member of a struct individually results in a lot of typing work:

[source,c]
....
int main() {
    TDate d1;
    d1.day = 13;
    strcpy(d1.month,"JAN");
    d1.year = 2000;
    ...
}
....

Just as you can initialize arrays using the curly brace syntax, you can also initialize structs with it:

[source,c]
....
TDate d1 = {13, "JAN", 2000};
....

This is much shorter and more convenient as you can use string initializers like "JAN" as well.

[NOTE]
====
Members must be initialized in the same order as they are defined in the structure!
====

When initializing nested structures, use nested groups of curly braces:

[source,c]
....
TPerson p1 = {{{"Alice", "May"}, "Howard"}, {15, "MAY", 1972}};
....

Each group of braces initializes one member.

[source,c]
....
TPerson p1 = {
    {                       // name member
        {"Alice", "May"},   // firstname array
        "Howard"
    },
    {15, "MAY", 1972}       // birthdate member
};
....

If you are very careful and specify values for all members (and in this case, values for all array elements), you can leave out the nested braces:

[source,c]
....
TPerson p1 = {"Alice", "May", "", "Howard", 15, "MAY", 1972};
....

[WARNING]
====
Avoid that syntax -- it does not make your program better or faster! Rather be more clear about what you initialize.
====

If you do not want to initialize all members or you want to initialize them in a different order, you can use the label-based syntax variant:

[source,c]
....
TPerson p1 = {
    .birthdate = {15, "MAY", 1972},
    .name = {{"Alice", "May"}, "Howard"}
};

TPerson p2 = {.birthdate = {17, "JUN", 1990}};
....

Explanation from a compiler manual:

[quote, GCC user manual]
____
In a structure initializer, specify the name of a field to initialize with '.fieldname =' before the element value. For example, given the following structure,

  struct point { int x, y; };

the following initialization

  struct point p = { .y = yvalue, .x = xvalue };

is equivalent to

  struct point p = { xvalue, yvalue };
____




==== Structs as Array Elements

Of course, you can define an array of variables that are structs.

Example:

[source,c]
....
int main() {
    TPerson clazz[30]; <1>
    int i;
    for(i = 0; i < 30; i++) {
        fgets(clazz[i].name.firstname[0],21,stdin); <2>
        fgets(clazz[i].name.lastname,41,stdin);
        scanf("%d", &clazz[i].birthdate.day); <3>
        fgets(clazz[i].birthdate.month);
        scanf("%", &clazz[i].birthdate.year);
    }
}
....
<1> `clazz` is an array of 30 `TPerson` objects. (Note: `class` is a reserved keyword in most compilers that also know C++).
<2> When accessing the array, do not forget to add an array index like `[i]` to first address one of the 30 array elements. Afterwards, continue with the dot syntax to access the structure's members like `name`.
<3> Do not forget to use the address-of operator when passing an integer variable like `day` to `scanf`!

In memory, you can imagine `clazz` like that:

[graphviz,generated_structs_personarray,svg]
....
digraph struct {
    rankdir=LR;
    node[shape=record,fontname=Helvetica];
    person [label="{<f0>[0]|{{TName name|{<f1>char firstname[3][21]|char lastname[41]}}|{TDate birthdate |{<f2>char day|<f3>char month[4]|<f4>int year}}}}|{[1]|{{TName name|{<f1>char firstname[3][21]|char lastname[41]}}|{TDate birthdate|{<f2>char day|<f3>char month[4]|<f4>int year}}}}|...|{[29]|{{TName name|{<f1>char firstname[3][21]|char lastname[41]}}|{TDate birthdate|{<f2>char day|<f3>char month[4]|<f4>int year}}}}"];
    clazz -> person:f0;
}
....

In total, `clazz` would have 3390 bytes (113 bytes for one `TPerson` * 30 array elements).
